# mtso 2017

def reverser
  yield.split.map{ |word| word.split('').reverse.join('') }.join(' ')
end

def adder(increment = 1)
  yield + increment
end

def repeater(times = 1)
  (0...times).each{ yield }
end
