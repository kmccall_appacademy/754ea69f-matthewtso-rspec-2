require 'time'

def measure(times = 1)
  start = Time.now
  (0...times).each{ yield }
  (Time.now - start) / times
end
